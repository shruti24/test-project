package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TeaKettlesPage {
	
	
	
	@FindBy(linkText="Krups Gooseneck Electric Kettle")
	public WebElement productKettle;
	
	public TeaKettlesPage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}

}
