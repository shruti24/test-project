package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;




public class HomePage {

	
	@FindBy(xpath="//a[contains(@href,'https://www.williams-sonoma.com/shop/cookware/')]")
	public WebElement cookWare;
	
	@FindBy(linkText="Tea Kettles")
	public WebElement teaKettles;
	
	@FindBy(xpath="//div[@class='email-campaign-wrapper-sticky joinEmailList']")
	public WebElement popUp;
	
	@FindBy(xpath="//a[@class='stickyOverlayMinimizeButton']")
	public WebElement cancelBtn;
	
	public HomePage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
}
