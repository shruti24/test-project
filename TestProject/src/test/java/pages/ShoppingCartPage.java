package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShoppingCartPage {
	
	
	@FindBy(xpath="//a[contains(@href,'https://www.williams-sonoma.com/shoppingcart/')]")
	public WebElement cartBtn;
	
	@FindBy(xpath="//a[@class='moveToSFL save-for-later-link']")
	public WebElement saveForLaterLink;
	
	@FindBy(xpath="//div[contains(text(),'Krups Gooseneck Electric Kettle')]")
	public WebElement itemTxt;
	
	
	
	
	
	public ShoppingCartPage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}

}
