package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ElectricKettlePage {

	
	
	@FindBy(xpath="//h1[contains(text(),'Krups Gooseneck Electric Kettle')]")
	public WebElement productKettleName;
	
	@FindBy(xpath="//span[contains(text(),'Add to Cart')]")
	public WebElement addToCartBtn;
	
	@FindBy(xpath="//a[contains(text(),'Checkout')]")
	public WebElement checkOutBtn;
	
	
	
	public ElectricKettlePage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	
}
