package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class YourAccountPage {
	

	@FindBy(id="email")
	public WebElement userNameTxtBx;
	
	@FindBy(id="password")
	public WebElement pwdTxtBx;
	
	@FindBy(css="button[class='btn btn-primary btn-mondo']")
	public WebElement signInBtn;
	
	
	
	public YourAccountPage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}

}
