package tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;



import pages.ElectricKettlePage;
import pages.HomePage;
import pages.ShoppingCartPage;
import pages.TeaKettlesPage;
import pages.YourAccountPage;

public class Ws_test {
	
	
		@Test
		 public void testAddingItemToCard() throws InterruptedException {
			System.setProperty("webdriver.chrome.driver",".\\Drivers\\chromedriver.exe");
			 WebDriver driver = new ChromeDriver();
			 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			  driver.get("https://www.williams-sonoma.com");
			  driver.manage().window().maximize();
		
			  HomePage hp= new HomePage(driver);
			  hp.cookWare.click();
			  
			 Thread.sleep(5000);
			    
			  			label:{  
				           if(hp.popUp.isDisplayed()) {
			  					
			  				WebDriverWait wait = new WebDriverWait(driver, 10);
			  				  wait.until(ExpectedConditions.visibilityOf(hp.cancelBtn));
					  		 hp.cancelBtn.click();
			  			  }
			                break label;
		
			  			  }
			  			
			  JavascriptExecutor jse = (JavascriptExecutor) driver;
			  jse.executeScript("window.scrollBy(0,500)", "");
			  WebDriverWait wait = new WebDriverWait(driver, 10);
				  wait.until(ExpectedConditions.visibilityOf(hp.teaKettles));
	          
			  hp.teaKettles.click();
			    JavascriptExecutor jse1 = (JavascriptExecutor) driver;
			    jse1.executeScript("window.scrollBy(0,700)", "");
			    
			    TeaKettlesPage tkp= new TeaKettlesPage(driver);
			    tkp.productKettle.click();
			    String productURL=driver.getCurrentUrl();
				System.out.println(productURL);
		
			
			     ElectricKettlePage ekp = new ElectricKettlePage(driver);
			    wait.until(ExpectedConditions.visibilityOf(ekp.productKettleName));
			     
			    String productName=ekp.productKettleName.getText();
			    System.out.println(productName);
			    
			    jse.executeScript("window.scrollBy(0,600)", "");
			    wait.until(ExpectedConditions.elementToBeClickable(ekp.addToCartBtn));
			    ekp.addToCartBtn.click();
			    
			    wait.until(ExpectedConditions.elementToBeClickable(ekp.checkOutBtn));
			    ekp.checkOutBtn.click();
			    
			    jse.executeScript("window.scrollBy(0,600)", "");
			   
			    YourAccountPage yap = new YourAccountPage(driver);
			    
			    wait.until(ExpectedConditions.visibilityOf(yap.userNameTxtBx));
			    yap.userNameTxtBx.sendKeys("shruti_dby@yahoo.com");
			    
			    wait.until(ExpectedConditions.visibilityOf(yap.pwdTxtBx));
			    yap.pwdTxtBx.sendKeys("automation123");


			    wait.until(ExpectedConditions.elementToBeClickable(yap.signInBtn));
			    yap.signInBtn.click();
			   
			    
			    ShoppingCartPage scp = new ShoppingCartPage(driver);
			    wait.until(ExpectedConditions.elementToBeClickable(scp.cartBtn));
			    scp.cartBtn.click();
			    
				    jse.executeScript("window.scrollBy(0,800)", "");
				    
				    wait.until(ExpectedConditions.elementToBeClickable(scp.saveForLaterLink));
				    scp.saveForLaterLink.click();
				    
				    
				    String currentURL= driver.getCurrentUrl();
				    System.out.println(currentURL);
				  
				    jse.executeScript("window.scrollBy(0,800)", "");
				    
				    wait.until(ExpectedConditions.visibilityOf(scp.itemTxt));
				    String itemName=  scp.itemTxt.getText();
				    System.out.println(itemName);
			
				  Assert.assertEquals(productName, itemName);
				  System.out.println("Saved Item Verified");
				  
				  driver.quit();
		
		
			  }
			  }

